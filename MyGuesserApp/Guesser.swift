//
//  guesser.swift
//  MyGuesserApp
//
//  Created by Kolakani,Satyakanth on 2/27/19.
//  Copyright © 2019 Kolakani,Satyakanth. All rights reserved.
//

import Foundation
struct Guess
{
    var correctAnswer : Int
    var numAttemptsRequired :Int
}


class Guesser
{
    static let shared = Guesser()
    private var correctAnswer : Int = 0
    private var _numAttempts : Int = 0
    private var guesses : [Guess] = []
    
    private init()
    {
        
    }
    
    var numAttemps : Int
    {
        return _numAttempts
    }
    
    func createNewProblem()
    {
        let pickrandom = Int.random(in: 1...10);
        self.correctAnswer = pickrandom
        self._numAttempts = 0
    }
    
    func amIRight(guess:Int) -> String
    {
        if guess != correctAnswer && guess < correctAnswer{
            _numAttempts = _numAttempts + 1
            return Result.tooLow.rawValue
        }
        else if guess != correctAnswer && guess > correctAnswer{
            _numAttempts = _numAttempts + 1
            return Result.tooHigh.rawValue
        }
        else
        {
            _numAttempts = _numAttempts + 1
            guesses.append(Guess(correctAnswer: correctAnswer, numAttemptsRequired: _numAttempts))
            return Result.correct.rawValue
        }
    }
    
    func guess(index:Int) -> Guess{
        return guesses[index]
    }
    
    func numGuesses() -> Int {
        return guesses.count
    }
    
    func minimumNumberOfAttempts() ->
        Int
        
    {
        if guesses.count == 0
        {
            return 0
        }
        else
        {
        var minimum : Int = guesses[0].numAttemptsRequired
        for i in 0 ... guesses.count-1
        {
            if guesses[i].numAttemptsRequired < minimum
            {
                minimum = guesses[i].numAttemptsRequired
            }
        }
        return minimum
        }
    }
    
    func maximumNumberOfAttempts() ->
        Int
    {
        if guesses.count == 0
        {
            return 0
        }
        else
        {
        var maximum : Int = guesses[0].numAttemptsRequired
        for i in 0 ... guesses.count-1
        {
            if guesses[i].numAttemptsRequired > maximum
            {
                maximum = guesses[i].numAttemptsRequired
            }
        }
        return maximum
        }
    }
    
    func Mean() ->
        Double{
            if guesses.count == 0
            {
                return 0
            }
            else
            {
        var average : Double = 0.0
        for i in 0 ... guesses.count-1
        {
            average = average + Double(guesses[i].numAttemptsRequired)
        }
        return average/Double(guesses.count)
            }
    }
    
    func standardDeviation() ->
        Double
    {
        if guesses.count == 0
        {
            return 0
        }
        else
        {
        var sd : Double = 0.0
        for i in 0 ... guesses.count-1
        {
            sd = sd + pow((Double(guesses[i].numAttemptsRequired) - Mean()),2)
        }
        return sqrt(sd/Double(guesses.count))
        }
    }
    
    func clearStatistics(){
        guesses = []
    }
    
}



enum Result:String {case tooLow = "Too Low", tooHigh = "Too High", correct = "Correct"}
