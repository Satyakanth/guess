//
//  ThirdViewController.swift
//  MyGuesserApp
//
//  Created by Kolakani,Satyakanth on 2/27/19.
//  Copyright © 2019 Kolakani,Satyakanth. All rights reserved.
//

import UIKit

class ThirdViewController: UIViewController {

    @IBOutlet weak var minimum: UILabel!
    
    
    
    @IBOutlet weak var maximum: UILabel!
    
    
    
    @IBOutlet weak var meanval: UILabel!
    
    
    
    
    @IBOutlet weak var stdDevi: UILabel!
    
    
    
    
    @IBAction func Stats(_ sender: Any)
    {
        
        let guess1 = Guesser.shared
        minimum.text = ""
        maximum.text = ""
        meanval.text = ""
        stdDevi.text = ""
        guess1.clearStatistics()
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        minimum.text = "\(Guesser.shared.minimumNumberOfAttempts())"
        maximum.text = "\(Guesser.shared.maximumNumberOfAttempts())"
        meanval.text = String(format : "%0.1f",(Guesser.shared.Mean()))
        stdDevi.text = String(format : "%0.1f",(Guesser.shared.standardDeviation()))

        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        viewDidLoad()
    }
    

    
}
