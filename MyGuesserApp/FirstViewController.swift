//
//  FirstViewController.swift
//  MyGuesserApp
//
//  Created by Kolakani,Satyakanth on 2/27/19.
//  Copyright © 2019 Kolakani,Satyakanth. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {

    @IBOutlet weak var myguessvalue: UITextField!
   
    @IBOutlet weak var rangeviewer: UILabel!
    
    @IBAction func DeterValue(_ sender: Any)
    {
        
       
        
        if let guessValue = Int(myguessvalue.text!)
        {
            if guessValue > 10
            {
               errorMessage()
            }
            else if guessValue < 1
            {
               errorMessage()
            }
            else
            {
                let result = Guesser.shared.amIRight(guess : guessValue)
                if(result == "Correct")
                {
                    rangeviewer.text = "\(result)"
                    displayMessage()
                    Guesser.shared.createNewProblem()
                }
                else
                {
                    rangeviewer.text = "\(result)"
                }
            }
        }
        else
        {
            errorMessage()
        }
        
        
        
    }
    func displayMessage(){
        let guess = Guesser.shared
        let alert = UIAlertController(title: "Well done",
                                      message: "You got it in \(guess.numAttemps) tries",
            preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default,
                                      handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func errorMessage(){
        let alert = UIAlertController(title: "Wrong Input",
                                      message: "Please enter a valid number",
            preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default,
                                      handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func newSolution(_ sender: Any)
    {
        myguessvalue.text = "0"
        rangeviewer.text = ""
       Guesser.shared.createNewProblem()
        
        }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Guesser.shared.createNewProblem()
        // Do any additional setup after loading the view, typically from a nib.
    }


}

